package com.kryviak.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main2 {

    public static final int RANDOM_BOUNDED = 100;
    public static final int LIST_LENGHT = 10;

    public static void main(String[] args) {
        findMinAndMaxValue(createList());
        findAverage(createList());
        newElement(createList());

    }

    private static List<Integer> createList() {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < LIST_LENGHT; i++) {
            int a = random.nextInt(RANDOM_BOUNDED);
            list.add(a);
        }
        return list;
    }

    private static List<Integer> findMinAndMaxValue(List<Integer> list) {
        Optional<Integer> maxValue = list.stream().max(Integer::compareTo);
        System.out.println("MaxValue=" + maxValue);
        int max = maxValue.get();
        Optional<Integer> minValue = list.stream().min(Integer::compareTo);
        System.out.println("MinValue=" + minValue);
        int min = minValue.get();

        IntSummaryStatistics stat = IntStream.range(min, max + 1)

                .summaryStatistics();
        System.out.println(stat);
        return list;
    }


    private static void findAverage(List<Integer> list) {
        IntSummaryStatistics stats = list
                .stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();

        stats.getSum();
        stats.getCount();
        System.out.println("Average is : " + stats.getAverage());
    }


    private static void newElement(List<Integer> list) {
        System.out.println("sum by using Stream : " + list.stream().mapToInt(Integer::intValue).sum());
        System.out.println("count by using Stream: " + list.stream().mapToInt(Integer::intValue).count());
        System.out.println("average by using Stream : " + list.stream().mapToInt(Integer::intValue).average());
        System.out.println("sort by using Stream: " + list.stream().sorted().collect(Collectors.toList()));
    }

}
