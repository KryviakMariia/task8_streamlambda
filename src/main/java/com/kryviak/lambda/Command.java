package com.kryviak.lambda;

@FunctionalInterface
public interface Command {
    void command (String name);
}
