package com.kryviak.lambda;
@FunctionalInterface
public interface Operationable {
    int findValue(int x, int y, int z);
}
