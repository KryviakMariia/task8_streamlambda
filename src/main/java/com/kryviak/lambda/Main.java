package com.kryviak.lambda;

import java.util.*;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        useCommand();

    }

    private static void useCommand() {
        System.out.println("Please enter method(eg:'name' or 'method':");
        Command com = name -> System.out.println(name);
        Command com2 = name -> findMaxAndAverageValue();
        Command com3 = name -> new Main();
        Command com4 = new Command() {
            @Override
            public void command(String name) {
                System.out.println("Object of command class");
            }
        };

        String value = scanner.next();
        switch (value) {
            case "name":
                com.command("First");
                break;
            case "method":
                com2.command("Second");
                break;
            case "class":
                com3.command("Third");
                System.out.println("You created anonymous class");
                break;
            case "object":
                com4.command("Four");
                break;
        }
    }

    private static void findMaxAndAverageValue() {
        Operationable operationable = (x, y, z) -> {
            int max;
            if (x > y && x > z) max = x;
            else if (y > x && y > z) max = y;
            else max = z;
            return max;
        };

        System.out.println("Please enter three values to find max:");
        int result = operationable.findValue(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        System.out.println("Max value is: " + result);

        System.out.println("Please enter three values to calculate average:");
        Operationable operationable1 = (x, y, z) -> (x + y + z) / 3;
        System.out.println("Average value is: " + operationable1.findValue(scanner.nextInt(), scanner.nextInt(), scanner.nextInt()));
    }

}
